'use strict';

var util = require('util');
var _ = require('lodash');
var sinon = require('sinon');

var Calculator = require('../index').Calculator;

describe('Calculator lib tests', function () {
    beforeEach(function () {
        this.calc = new Calculator();
    });

    afterEach(function () {
        this.calc = null;
    });

    describe('#pow/positive', function () {
       it('should validate arguments', function() {
           this.calc.pow.bind(this.calc, null, 1).should.throw(/Argument error/);
       });
    });

    describe('#pow/positive', function () {
        it('should power numbers', function () {
            this.calc.pow(2, 1).should.not.eql(0).and.eql(2);
            this.calc.pow(-12, 2).should.not.eql(12).and.eql(144);
            this.calc.pow(2, 8).should.not.eql(64).and.eql(256);
        });
    });

    describe('#pow/negative', function () {
        it("shouldn't power numbers", function () {
            this.calc.pow.bind(this.calc, '2', 0).should.throw(/Argument error/);
            this.calc.pow.bind(this.calc, {}, {}).should.throw();
        });
    });

    describe('#multiply/positive', function () {
        it('should multiply integers/floats', function () {
            this.calc.multiply(12, 10).should.eql(120);
            this.calc.multiply(2, 0.2).should.eql(0.4);
            this.calc.multiply(0.3, 0.1).should.eql(0.03);
        });
    });

    describe('positive/dataset', function () {
        var tests = [
            {args: [1, 2],       expected: 3},
            {args: [-1, 2],      expected: 1},
            {args: [1, -2],      expected: -1},
            {args: [0, 0],       expected: 0}
        ];

        tests.forEach(function(test) {
            it('should correctly add ' + test.args.length + ' args', function() {
                this.calc.add(test.args[0], test.args[1]).should.eql(test.expected);
            });
        });
    })
});
